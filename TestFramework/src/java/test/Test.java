/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import webService.annotation.ReferenceUrl;
import webService.model.ModelView;

/**
 *
 * @author P14A-01-Faneva
 */
public class Test {
    
    @ReferenceUrl(url = "Andrana")
    public ModelView getAndrana(){
        ModelView model = new ModelView();
        model.setRedirect("Andrana.html");
        return model;
    }
    
}
